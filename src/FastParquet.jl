module FastParquet

using PyCall, Tables
import Pandas

const fastparquet = PyNULL()


__init__() = copy!(fastparquet, pyimport("fastparquet"))

ParquetFile(fname; kw...) = fastparquet.ParquetFile(fname, kw...)

function read(fname, cols=String[]; kw...)
    f = ParquetFile(fname; kw...)
    Pandas.DataFrame(isempty(cols) ? f.to_pandas() : f.to_pandas(string.(cols)))
end

_write(fname::AbstractString, data::PyObject; kw...) = fastparquet.write(fname, data)
function _write(fname::AbstractString, data::Pandas.DataFrame; kw...)
    _write(fname, data.pyo; kw...)
end

write(fname::AbstractString, data; kw...) = _write(fname, Pandas.DataFrame(data); kw...)


end
