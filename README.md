# FastParquet

This is a "quick and dirty" wrapper of [`fastparquet`](https://github.com/dask/fastparquet).  It uses [Pandas.jl](https://github.com/JuliaPy/Pandas.jl) to translate the format.

This is used mainly for testing and validation.  For native Julia parquet packages see [Parquet.jl](https://github.com/JuliaIO/Parquet.jl]) and
[Parquet2.jl](https://gitlab.com/ExpandingMan/Parquet2.jl).

## Usage
```julia
import FastParquet
using DataFrames

df = DataFrame(FastParquet.read("file.parq"))

FastParquet.write("file1.parq", df)
```

!!! warning

    You will need to do `pip install fastparquet` because I haven't set it up as a dependency
    because this isn't a real package.
